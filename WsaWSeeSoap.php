<?php

/**
 * @author     Robert Richards <rrichards@ctindustries.net>
 * @copyright  2007 Robert Richards <rrichards@ctindustries.net>
 * @license    http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @version    1.0.0
 * +logowanie
 */
class WsaWSeeSoap
{

    const WSANS = 'http://www.w3.org/2005/08/addressing';
    const OASIS = 'http://docs.oasis-open.org/wss/2004/01';

    const WSAPFX = 'wsa';
    const WSEEPFX = 'wsee';

    private $soapNS, $soapPFX;
    private $soapDoc = NULL;
    private $envelope = NULL;
    private $SOAPXPath = NULL;
    private $header = NULL;
    private $messageID = NULL;


    private function locateHeader()
    {

        if ($this->header == NULL) {
            $headers = $this->SOAPXPath->query('//wssoap:Envelope/wssoap:Header');
            $header = $headers->item(0);
            if (!$header) {
                $header = $this->soapDoc->createElementNS($this->soapNS, $this->soapPFX . ':Header');
                $this->envelope->insertBefore($header, $this->envelope->firstChild);
            }

            $this->header = $header;
        }
        return $this->header;
    }

    public function __construct($doc)
    {

        $this->soapDoc = $doc;
        $this->envelope = $doc->documentElement;
        $this->soapNS = $this->envelope->namespaceURI;
        $this->soapPFX = $this->envelope->prefix;
        $this->SOAPXPath = new DOMXPath($doc);
        $this->SOAPXPath->registerNamespace('wssoap', $this->soapNS);
        $this->SOAPXPath->registerNamespace('wswsa', WsaWSeeSoap::WSANS);

        $this->envelope->setAttributeNS("http://www.w3.org/2000/xmlns/", 'xmlns:' . WsaWSeeSoap::WSAPFX, WsaWSeeSoap::WSANS);

        $header = $this->locateHeader();
        $header->setAttributeNS("http://www.w3.org/2000/xmlns/", 'xmlns:' . WsaWSeeSoap::WSAPFX, WsaWSeeSoap::WSANS);

    }

    public function setAuthHeader($username, $password)
    {
        $header = $this->locateHeader();

        $nodeSecurity = $this->soapDoc->CreateElement(WsaWSeeSoap::WSEEPFX . ':Security');
        $domAttribute = $this->soapDoc->CreateAttribute('xmlns:' . WsaWSeeSoap::WSEEPFX);
        $domAttribute2 = $this->soapDoc->CreateAttribute('xmlns:wsu');
        $domAttribute->value = WsaWSeeSoap::OASIS . '/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        $domAttribute2->value = WsaWSeeSoap::OASIS . '/oasis-200401-wss-wssecurity-utility-1.0.xsd';
        $nodeSecurity->appendChild($domAttribute);

        $nodeSecurity->appendChild($domAttribute2);

        $header->appendChild($nodeSecurity);

        $nodeUserNameToken = $this->soapDoc->CreateElement(WsaWSeeSoap::WSEEPFX . ':UsernameToken');

        $domAttribute = $this->soapDoc->CreateAttribute('wsu:Id');
        $domAttribute->value = 'UsernameToken-44';

        $nodeUserNameToken->appendChild($domAttribute);

        $nodeSecurity->appendChild($nodeUserNameToken);

        $nodeAction = $this->soapDoc->CreateElement(WsaWSeeSoap::WSEEPFX . ':Username', $username);
        $nodeUserNameToken->appendChild($nodeAction);

        $nodeAction = $this->soapDoc->CreateElement(WsaWSeeSoap::WSEEPFX . ':Password', $password);

        $domAttribute = $this->soapDoc->CreateAttribute('Type');
        $domAttribute->value = WsaWSeeSoap::OASIS . '/oasis-200401-wss-username-token-profile-1.0#PasswordText';

        $nodeUserNameToken->appendChild($domAttribute);
        $nodeUserNameToken->appendChild($nodeAction);

        $nodeSecurity->appendChild($nodeUserNameToken);

    }

    public function addAction($action, $location)
    {

        $header = $this->locateHeader();

        $nodeAction = $this->soapDoc->createElementNS(WsaWSeeSoap::WSANS, WsaWSeeSoap::WSAPFX . ':Action', $action);
        $header->appendChild($nodeAction);

        $nodeTo = $this->soapDoc->createElementNS(WsaWSeeSoap::WSANS, WsaWSeeSoap::WSAPFX . ':To', $location);
        $header->appendChild($nodeTo);

    }


    public function getDoc()
    {
        return $this->soapDoc;
    }

    public function saveXML()
    {
        return $this->soapDoc->saveXML();
    }

    public function save($file)
    {
        return $this->soapDoc->save($file);
    }
}

