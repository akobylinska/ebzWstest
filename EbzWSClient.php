<?php

require 'WSAWSEESoap.php';

class EbzWsClient extends SoapClient
{

    private $username;
    private $password;
    private $filename;
    
    function __construct($wsdl, array $options = null, $username, $password)
    {
        $this->username = $username;
        $this->password = $password;
        parent::__construct($wsdl, $options);

    }
    
    function saveAs($filename) {
        $this->filename = $filename;
    }


    function __doRequest($request, $location, $action, $version, $one_way = NULL)
    {


        $doc = new DOMDocument();
        $doc->loadXML($request);

        $wsa = new WsaWSeeSoap($doc);
        $wsa->setAuthHeader($this->username, $this->password);
        $wsa->addAction($action, $location);

        $request = $wsa->getDoc()->saveXML();

        $headers = [
            "Content-type:application/soap+xml;charset=utf-8",
        ];

     $save_file = strlen($this->filename);

        $ch = curl_init();
        $fp = null;

        if($save_file)
            $fp = fopen($this->filename, "w+");

       $options = [
            CURLOPT_URL => $location,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POSTFIELDS => $request,
        ];
        if($save_file)
            $options[CURLOPT_FILE] = $fp;

        curl_setopt_array($ch, $options);

       $results =  curl_exec($ch);

        if ($fp != null)
            fclose($fp);


        curl_close($ch);
        return $results;

    }



}

?>